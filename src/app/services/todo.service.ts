import { Injectable } from '@angular/core';
import { Todo } from 'app/model/todo';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class TodoService {
    public findAll(): Promise<Array<Todo>> {
        return new Promise<Array<Todo>>((resolve) => {
            const todos = [
                new Todo({ done: false, text: 'TODO #1' }),
                new Todo({ done: true, text: 'TODO #2' })
            ];
            resolve(todos);
        });
    }

    public findAllObservable(): Observable<Array<Todo>> {
        return new Observable(observer => {
            const todos = [
                new Todo({ done: false, text: 'TODO #1' }),
                new Todo({ done: true, text: 'TODO #2' })
            ];
            observer.next(todos);
            observer.complete();
        });
    }
}
