import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {ComponentsModule} from 'app/ui/components/components.module';
import {TodoListComponent} from 'app/ui/pages/todo-list/todo-list.component';
import {TodoService} from 'app/services/todo.service';

@NgModule({
    declarations: [
        TodoListComponent
    ],
    imports: [
        BrowserModule, ComponentsModule
    ],
    providers: [TodoService],
})
export class TodoListModule { }
