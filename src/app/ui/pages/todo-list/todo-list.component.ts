import {Component, OnInit} from '@angular/core';
import {TodoService} from 'app/services/todo.service';
import {TodoViewItem} from '../../view-item/todo.view-item';
import * as _ from 'lodash';

@Component({
    selector: 'app-todo-list',
    templateUrl: 'todo-list.html'
})
export class TodoListComponent implements OnInit {

    public todos: Array<TodoViewItem> = [];

    constructor(private service: TodoService) {
    }

    ngOnInit(): void {
        this.todos = [];
        this.service.findAllObservable()
            .subscribe(
                todos => {
                    const viewItems = _.map(todos, t => new TodoViewItem(t));
                    this.todos = this.todos.concat(viewItems);
                },
                error => {
                    console.error(error);
                },
                () => {
                    console.log('All done');
                }
            );
    }
}
