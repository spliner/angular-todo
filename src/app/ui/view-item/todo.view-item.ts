import {Todo} from '../../model/todo';

export class TodoViewItem {
    constructor(private _model: Todo) {
    }

    get model(): Todo {
        return this._model;
    }

    get text(): string {
        return this._model.text;
    }

    set text(value: string) {
        this._model.text = value;
    }

    get done(): boolean {
        return this._model.done;
    }

    set done(value: boolean) {
        this._model.done = value;
    }

    get highlightColor(): string {
        return this.done ? 'green' : '';
    }
}
