import {Component, Input} from '@angular/core';
import {TodoViewItem} from '../../view-item/todo.view-item';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Todo} from '../../../model/todo';
import {EditTodoModalComponent} from '../edit-todo-modal/edit-todo-modal.component';

@Component({
    selector: 'app-todo',
    templateUrl: 'todo.html'
})
export class TodoComponent {
    @Input()
    public todo: TodoViewItem;

    constructor(private modalService: NgbModal) {
    }

    public edit(): void {
        const todoClone = new Todo(this.todo.model);
        const todoViewItemClone = new TodoViewItem(todoClone);

        const modalReference = this.modalService.open(EditTodoModalComponent);
        modalReference.componentInstance.todo = todoViewItemClone;
        modalReference.result
            .then(todo => {
                this.todo = todo;
            }, () => {
                console.log('Canceled');
            })
            .catch(() => {
                console.log('Error');
            });
    }
}
