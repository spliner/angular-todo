import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {TodoViewItem} from '../../view-item/todo.view-item';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {EditTodoComponent} from '../edit-todo/edit-todo.component';

@Component({
    selector: 'app-edit-todo-modal',
    templateUrl: './edit-todo-modal.component.html'
})
export class EditTodoModalComponent implements OnInit {

    @Input()
    public todo: TodoViewItem;
    @ViewChild(EditTodoComponent)
    public editTodoComponent: EditTodoComponent;

    constructor(public activeModal: NgbActiveModal) { }

    ngOnInit() {
    }

    public save(): void {
        if (!this.editTodoComponent.form || !this.editTodoComponent.form.valid) {
            return;
        }

        this.activeModal.close(this.todo);
    }

    public cancel(): void {
        this.activeModal.dismiss();
    }
}
