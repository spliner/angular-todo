import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {TodoComponent} from 'app/ui/components/todo/todo.component';
import {DirectivesModule} from '../directives/directives.module';
import {EditTodoComponent} from './edit-todo/edit-todo.component';
import {EditTodoModalComponent} from './edit-todo-modal/edit-todo-modal.component';

@NgModule({
    declarations: [
        TodoComponent,
        EditTodoComponent,
        EditTodoModalComponent
    ],
    exports: [
        TodoComponent,
        EditTodoComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        DirectivesModule,
        NgbModule
    ],
    entryComponents: [EditTodoModalComponent],
    providers: [],
})
export class ComponentsModule { }
