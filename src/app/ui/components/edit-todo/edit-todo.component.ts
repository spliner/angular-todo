import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {TodoViewItem} from '../../view-item/todo.view-item';
import {NgForm} from '@angular/forms';

@Component({
    selector: 'app-edit-todo',
    templateUrl: './edit-todo.component.html'
})
export class EditTodoComponent implements OnInit {
    @Input()
    public todo: TodoViewItem;

    @ViewChild('todoForm')
    public form: NgForm;

    constructor() { }

    ngOnInit() {
    }
}
