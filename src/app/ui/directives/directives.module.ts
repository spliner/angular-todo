import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BoldDirective} from './bold.directive';
import {HighlightDirective} from './highlight.directive';

@NgModule({
    declarations: [
        BoldDirective, HighlightDirective
    ],
    exports: [
        BoldDirective, HighlightDirective
    ],
    imports: [
        BrowserModule
    ],
    providers: [],
})
export class DirectivesModule { }
