export class Todo {
    public text: string;
    public done: boolean;

    constructor(init?: Partial<Todo>) {
        Object.assign(this, init)
    }
}
