import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterModule} from '@angular/router';

import {AppComponent} from './app.component';

import {TodoListModule} from 'app/ui/pages/todo-list/todo-list.module';
import {TodoListComponent} from 'app/ui/pages/todo-list/todo-list.component';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        TodoListModule,
        NgbModule.forRoot(),
        RouterModule.forRoot([
            {
                path: 'todos',
                component: TodoListComponent
            },
            {
                path: '**',
                component: TodoListComponent
            }
        ])
    ],
    providers: [
    ],
    bootstrap: [AppComponent],
})
export class AppModule { }
